<!doctype html>
<html lang="en">

    <?php $this->load->view("widgets/head.php"); ?>

    <body class="uk-grid">
        <?php $this->load->view("widgets/header.php"); ?>
        <?php $this->load->view("widgets/nav-aside.php"); ?>

        <main class="uk-width-8-10 uk-container uk-container-center">
            
            <div class="uk-grid">
                <div class="uk-width-1-2"><h1>Edit: <?= $article['title']; ?></h1></div>
                <div class="uk-width-1-2 uk-text-right"><p><a href="<?= base_url("articles"); ?>" class="uk-icon-justify uk-icon-close" style="font-size: 40px;"></a></p></div>
            </div>
            <div class="uk-grid">
                <form class="uk-form uk-width-2-3" method="post" action="<?= base_url("articles/edit/{$article['id']}"); ?>">
                    <div class="uk-form-row">
                        <input type="text" name="article-title" id="article-title" placeholder="Title" value="<?= $article['title'];?>">
                    </div>
                    <div class="uk-form-row">
                        <input type="text" name="article-alias" id="article-alias" placeholder="Alias" value="<?= $article['alias']; ?>">
                    </div>
                    <div class="uk-form-row">
                        <textarea rows="2" name="article-intro" id="article-intro" placeholder="intro"><?= $article['intro']; ?></textarea>
                    </div>
                    <div class="uk-form-row">
                        <textarea rows="10" name="article-body" id="article-body" placeholder="body"><?= $article['body']; ?></textarea>
                    </div>
                    <div class="uk-form-row">
                        <input type="text" name="article-author" id="article-author" placeholder="Author" value="<?= $article['author']; ?>">
                    </div>
                    <input type="hidden" name="action" value="edit" />
                    <div class="uk-form-row uk-margin-top">
                        <button class="uk-button" type="submit">Submit</button>
                    </div>
                </form>
            </div>
            <?php $this->load->view("widgets/footer.php"); ?>
        </main>
        
    </body>
</html>

