<!doctype html>
<html lang="en">

    <?php $this->load->view("widgets/head.php"); ?>

    <body class="uk-grid">
        <?php $this->load->view("widgets/header.php"); ?>
        <?php $this->load->view("widgets/nav-aside.php"); ?>

        <main class="uk-width-8-10 uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-2"><h1 class="uk-margin-left">Trashed articles</h1></div>
                <div class="uk-width-1-2 uk-text-right">
                    <p class="uk-margin-right">
                        <a href="<?= base_url('articles/create') ?>" class="uk-icon-justify uk-icon-plus" style="font-size: 40px;"></a>
                    </p>
                </div>
            </div>

            <table class="uk-table uk-table-striped uk-table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Intro</th>
                        <th>Author</th>
                        <th>Trashed</th>
                        <th>Tools</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($articles)) : ?>					
                        <?php foreach ($articles as $article) : ?>
                            <?php $article['date_trashed'] = date('d/m/Y', strtotime($article['date_trashed'])); ?>
                            <tr>
                                <td class="uk-width-1-10"><?= $article['id']; ?></td>
                                <td class="uk-width-3-10"><?= $article['title']; ?></td>
                                <td class="uk-width-3-10"><?= $article['intro']; ?></td>
                                <td class="uk-width-1-10"><?= $article['author']; ?></td>
                                <td class="uk-width-1-10"><?= $article['date_trashed']; ?></td>
                                <td class="uk-width-1-10">
                                    <a href="<?= base_url("articles/restore/{$article['id']}") ?>" class="uk-icon-justify uk-icon-rotate-left"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>

        </main>

        <?php $this->load->view("widgets/footer.php"); ?>

    </body>
</html>

