<head>
    <title>Zep Design | Digital Designer & Front-end Developer</title>
    <!--Meta-->
    <meta charset="utf-8">
    <meta name="robots" content="index, follow" />
    <meta name="viewport" content="initial-scale=1" />
    <meta name="keywords" content="Zep Design, Giuseppe Castiglione, Digital, Designer, Developer, London, Hertfordshire, Freelance, Back-end, Front-end" >
    <meta name="Description" content="Zep Design is the portfolio of Giuseppe Castiglione. A London & Hertfordshire based Freelance Designer and Developer.">
    <!--Facebook Meta-->
    <meta property="og:url" content="http://zep.design" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Zep Design - Digital Designer & Front-end Developer" />
    <meta property="og:description" content="Creative adventurer, helping people along the digital road in their journey. Offering creative advice, industry insight and commercial perspective." />
    <meta property="og:image" content="http://zep.design/img/zepdesign-logo.jpg" />
    <!--Twitter Meta-->
    <!--Assets-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,500' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?=URL?>theme/css/uikit.min.css">
    <link rel="stylesheet" href="<?=URL?>theme/css/style.css?version=<?= time();?>">

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>