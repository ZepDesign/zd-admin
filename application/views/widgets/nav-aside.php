<aside class="uk-width-2-10">
	<div class="logo uk-align-center">
		<a href="<?= URL ?>" title="Zep Design Homepage">
			<img src="<?= ASSETS ?>zep-design-logo.svg" alt="Zep Design Logo" />
		</a>
	</div>
    <ul class="uk-nav uk-nav-parent-icon" data-uk-nav="{multiple:true}">
        <li><a href="">Dashboard</a></li>
        <li><a href="">Frontpage</a></li>

        <li class="uk-parent">
            <a href="#">Articles</a>
            <ul class="uk-nav-sub">
                <li><a href="<?= URL ?>articles/">All Articles</a></li>
                <li><a href="<?= URL ?>articles/create">Add Article</a></li>
                <li><a href="<?= URL ?>articles/trashed">All Trashed</a></li>
            </ul>
        </li>
        <li class="uk-parent">
            <a href="#">Work</a>
            <ul class="uk-nav-sub">
                <li><a href="<?= URL ?>work/index">View All</a></li>
                <li><a href="<?= URL ?>work/create">Add Work</a></li>
            </ul>
        </li>
    </ul>
</aside>