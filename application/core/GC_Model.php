<?php

class GC_Model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	protected function fetch_by_id($table, $id) {
		$this->db->where('id', $id);
		$query = $this->db->get($table);
		return $query->row_array();
	}
	
	//add func - check if alias exists
	protected function generate_alias($title) {
		if (empty($title))
			return false;
		return (str_replace(' ', '-', strtolower($title)));
	}
	
	protected function trash_item($table, $id) {
		$data = array(
			'active' => 0,
			'date_trashed' => time()
		);
		$this->db->where('id', $id);
		return $this->db->update($table, $data);
	}
	
	protected function restore_item($table, $id) {
		$data = array(
			'active' => 1,
			'date_trashed' => 0,
			'last_update' => time()
		);	
		$this->db->where('id', $id);
		return $this->db->update($table, $data);
	}

}
