<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends GC_Controller {

	public function index() {
		$this->load->model('Article_model');
		$data['articles'] = $this->Article_model->all_articles();
		$this->load->view('articles/index', $data);
	}

	public function trashed() {
		$this->load->model('Article_model');
		$data['articles'] = $this->Article_model->all_trashed();
		$this->load->view('articles/trashed', $data);
	}

	public function create() {
		//get form data
		$title = $this->input->post('article-title');
		$intro = $this->input->post('article-intro');
		$alias = $this->input->post('article-alias');
		$body = $this->input->post('article-body');
		$author = $this->input->post('article-author');
		$action = $this->input->post('action');
		//check if $action is !empty and action == create

		if (!empty($action) && $action == 'create') {
			//if not empty, check title & body !empty load model
			if (!empty($title) && !empty($body)) {
				$this->load->model('Article_model');
			}
			//create var and pass form data var to create func in model | vars need to be in the same order in Article_model
			$article_id = $this->Article_model->create($title, $body, $alias, $intro, $author);
			//return $article_id if successfull, then redirect bac to controller controller (articles/index)
			if (!empty($article_id)) {
				redirect(base_url('articles'));
			}
		}

		$this->load->view('articles/create');
	}

	public function edit($id) {
		$data = array(); // why do we set an empty array?
		$this->load->model('Article_model');
		
		//Collect data to update post		
		$title = $this->input->post('article-title');
		$intro = $this->input->post('article-intro');
		$alias = $this->input->post('article-alias');
		$body = $this->input->post('article-body');
		$author = $this->input->post('article-author');
		$action = $this->input->post('action');

		if (!empty($action) || $action == 'edit') {
			$updated = $this->Article_model->edit($id, $title, $body, $alias, $intro, $author);
			
			if ($updated) {
				redirect(base_url('articles'));
			} else {
				redirect(base_url('article/edit'));
			}
		}
		
		//pull data and view article to edit
		$article = $this->Article_model->read($id);
		$data['article'] = $article;
		$this->load->view('articles/edit', $data);
	}

	public function trash($id) {
		$this->load->model('Article_model');

		if (!empty($id)) {
			$trashed = $this->Article_model->trash($id);

			if ($trashed) {
				redirect(base_url('articles'));
			} else {
				redirect(base_url('trash/error'));
			}
		}
	}

	public function restore($id) {
		$this->load->model('Article_model');

		if (!empty($id)) {
			$restored = $this->Article_model->restore($id);

			if ($restored) {
				redirect(base_url('articles'));
			} else {
				redirect(base_url('restore/error'));
			}
		}
	}

}
