<?php

class Article_model extends GC_Model {

	public function __construct() {
		parent::__construct();
	}

	private static $table = 'articles';

	public function all_articles() {
		//DB select all
		$this->db->select('*');
		//DB call from
		$this->db->from(self::$table);
		//DB where active (FUTURE)
		$this->db->where('active', 1);
		//DB order by [FIELD] in descending
		$this->db->order_by('date_created', 'DESC');
		//Send get request to DB, pass into a var
		$query = $this->db->get();

		//return var to result array, to be used in the view
		return $query->result_array();
	}

	public function all_trashed() {
		$this->db->select('*');
		$this->db->from(self::$table);
		$this->db->where('active', 0);
		$this->db->order_by('date_trashed', 'DESC');
		$query = $this->db->get();
		
		return $query->result_array();
	}

	//null so that the function isn't expecting there to be something when that information isn't available, (Eg, quick post feature with less fields)
	//Pass vars into function to put into db
	public function create($title, $body, $alias = null, $intro = null, $author = null) {
		//check if !null vars exist, if not return false
		if (empty($title) || empty($body)) {
			return false;
		}

		//if alias is empty generate one with a GC_Model function
		if (empty($alias)) {
			$alias = $this->generate_alias($title);
		}

		//pass vars into $data array, to pass into db table
		$data = array(
			'title' => $title,
			'intro' => $intro,
			'alias' => $alias,
			'body' => $body,
			'author' => $author,
			'last_update' => time()
		);
		//insert into table, make it var to check
		$done = $this->db->insert(self::$table, $data);
		//return id of new post if inserted
		return $done ? $this->db->insert_id() : false;
	}

	public function read($id) {
		return $this->fetch_by_id(self::$table, $id);
	}

	public function edit($id, $title, $body, $alias = null, $intro = null, $author = null ) {
		if (empty($id) || empty($title) || empty($body)) {
			return false;
		}
		
		$data = array(
			'title' => $title,
			'intro' => $intro,
			'alias' => $alias,
			'body' => $body,
			'author' => $author,
			'last_update' => time()
		);
		
		$this->db->where('id', $id);
		return $this->db->update(self::$table, $data);
	}

	public function trash($id) {
		return $this->trash_item(self::$table, $id);
	}

	public function restore($id) {
		return $this->restore_item(self::$table, $id);		
	}

}
