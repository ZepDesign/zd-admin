-- phpMyAdmin SQL Dump
-- version 4.4.15
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 28, 2017 at 02:55 PM
-- Server version: 5.5.19
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zd-admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `alias` varchar(200) NOT NULL,
  `intro` text NOT NULL,
  `body` text NOT NULL,
  `author` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_trashed` int(11) NOT NULL DEFAULT '0',
  `last_update` int(11) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `alias`, `intro`, `body`, `author`, `date_created`, `date_trashed`, `last_update`, `active`) VALUES
(1, 'Test post 1', '', 'This is the intro of my text', 'This is the body of my article', 'Zep', '2017-07-27 15:57:58', 0, 0, 1),
(2, 'Test Post 2', '', 'This is the second intro', 'My long awaited second body text', 'Zep', '2017-07-27 16:05:12', 0, 1501246364, 1),
(3, 'Test post 3 updated', 'test-post-3', 'This is test post 3 intro', 'this is the body content for article 3', 'Zep', '2017-07-28 08:56:13', 0, 1501253117, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('tdoqpvhroll88hu3p7mll5h80pivb2a6', '127.0.0.1', 1501167181, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313136373032383b),
('2vm2enjm0kepfb5mb6ps6f4bfdesgqfh', '127.0.0.1', 1501167681, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313136373532323b),
('r7695ecf8o1v60ivra7trddlmd5v2586', '127.0.0.1', 1501168302, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313136383330323b),
('mffdvqldvr5qbng5nn117tvitcidk8ls', '127.0.0.1', 1501168827, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313136383832373b),
('g1l1ca4r0mji5cj9uvh724a15mi5prjb', '127.0.0.1', 1501169764, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313136393532303b),
('mtah14p65oidd9uha8sbd0pl0idjm1u7', '127.0.0.1', 1501169968, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313136393834333b),
('heri0qal96g1s44vm0mlufa8dasaqov2', '127.0.0.1', 1501170546, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313137303238373b),
('edq0eic34g871jenkkj1gkkkcnbuibre', '127.0.0.1', 1501170914, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313137303634323b),
('bfmi6frrk3o4g0eo993k17df03g40dbe', '127.0.0.1', 1501171082, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313137303938333b),
('kpiees89nq51likvh2avt81t1v4ra0tk', '127.0.0.1', 1501171884, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313137313631383b),
('3mcr01qjsk334srmu8grk6kdoqdvhb5i', '127.0.0.1', 1501172218, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313137313938333b),
('urgofnn4j6h40ac1e4fjs53liha97obq', '127.0.0.1', 1501229889, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313232393736333b),
('8se4fduf5bodbkr5cv3cugnd0nsicu2g', '127.0.0.1', 1501230277, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313233303237373b),
('iac22ar2v1ocntent97tlh0d8d8tkr25', '127.0.0.1', 1501231144, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313233313034373b),
('q0pjj64ftc1en2vahq4jc8sml1iturgp', '127.0.0.1', 1501231823, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313233313538303b),
('mtd8vj9vb9i19lh0i2aqjhupf2oftjcv', '127.0.0.1', 1501232173, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313233313838393b),
('f9gc3uu8ku7n55g5c6rdk4acjlqm4g0u', '127.0.0.1', 1501232764, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313233323638363b),
('67mocl7i02nend35m9iqpe2o7dahoh2u', '127.0.0.1', 1501245005, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313234343939303b),
('lacflu1eomjve7s4khuoqvf57s06ub3i', '127.0.0.1', 1501245608, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313234353331353b),
('d0n4dkuv0f83sdu5cprbgpbrb12id243', '127.0.0.1', 1501245703, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313234353638373b),
('daactclmafi476s9057r2oit256cia3s', '127.0.0.1', 1501246378, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313234363239313b),
('tbenjs9de62q32beec2qb8j4kd0ianh9', '127.0.0.1', 1501247579, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313234373437383b),
('osd1ej8ge7bin6gohvnrtur9v3337pnd', '127.0.0.1', 1501252500, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313235323330333b),
('rgcfa9ftupdtmv5pmrd9ebmkmem0q80s', '127.0.0.1', 1501253394, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313235333039343b),
('ctn8abb2q6atqajtjjost746rb9tf0vs', '127.0.0.1', 1501253678, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313235333430373b),
('8ooop5g32l5dm0o7dju6sg4isfuqvi3h', '127.0.0.1', 1501253714, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530313235333731343b);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
